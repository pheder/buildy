import { Component, OnInit, OnDestroy, ViewEncapsulation, ElementRef, ViewChild } from '@angular/core';
import {Build} from '../../models/build';
import { UserService } from '../../services/user/user.service';
import { ComponentsService } from '../../services/components/components.service';

import {MatSnackBar} from '@angular/material';


@Component({
  selector: 'app-builds',
  templateUrl: './mybuilds.component.html',
  styleUrls: ['./mybuilds.component.scss'],
  encapsulation: ViewEncapsulation.None,
})

export class MyBuildsComponent implements OnInit, OnDestroy {

  loggedIn: boolean;
  build = new Build();
  validBuild: boolean;
  rows = [];
  loadingIndicator: boolean = true;
  finishedBuilds: any[];

  aux = [
    {type: "cpu", manufacturer: "", model : "", price: 0, _id : ""},
    {type: "cpu-cooler", manufacturer: "", model : "", price: 0, _id : ""},
    {type: "motherboard", manufacturer: "", model : "", price: 0, _id : ""},
    {type: "memory", manufacturer: "", model : "", price: 0, _id : ""},
    {type: "drive", manufacturer: "", model : "", price: 0, _id : ""},
    {type: "gpu", manufacturer: "", model : "", price: 0, _id : ""},
    {type: "case", manufacturer: "", model : "", price: 0, _id : ""},
    {type: "psu", manufacturer: "", model : "", price: 0, _id : ""},
  ];

  @ViewChild('name') name:ElementRef;
  @ViewChild('desc') desc:ElementRef;

  constructor(private userService: UserService, public snackBar: MatSnackBar) {
  }

  ngOnInit() {
    // Get user build from the database
    this.fetch((data) => {
      this.loadingIndicator = false;
      this.build.price = 0;
      if(!!data.build){
        this.rows.push({...this.aux[0], ...data.build.components.cpu}, {...this.aux[1], ...data.build.components.cpu_cooler},{...this.aux[2], ...data.build.components.motherboard}, {...this.aux[3], ...data.build.components.memory} );
        data.build.components.drive.forEach(drive => {
          drive = {...this.aux[4], ...drive};
          this.rows.push(drive)
        })
        this.rows.push({...this.aux[4]},{...this.aux[5], ...data.build.components.gpu},{...this.aux[6], ...data.build.components.case},{...this.aux[7], ...data.build.components.psu});
        this.build.name = data.build.name || "My awesome build";
        this.build.description = data.build.description || "";
        if(!data.build.components.cpu || !data.build.components.gpu || !data.build.components.psu || !data.build.components.case || !data.build.components.motherboard || !data.build.components.memory || !data.build.components.drive){
          this.validBuild = false;
        }
        else{
          this.validBuild = true;
        }
      }

      // Calculate total price
      this.rows = [...this.rows];
      this.rows.forEach(row =>{
        this.build.price += row.price;
      })
      this.name.nativeElement.value = this.build.name;
      this.desc.nativeElement.value = this.build.description;
    });
  }

  fetch(cb) {
    this.userService.getUserBuild().subscribe(data => {
    cb(data);
  });
}


  ngOnDestroy(){

  }

  // Delete component from the build
  deleteComponent(row) {
    this.userService.deleteBuildComponent(row.type, row._id).subscribe(res => {
      if (row.type == "drive") {
        let index = this.rows.findIndex(r => {
          return r._id == row._id;
        });
        this.rows.splice(index, 1);
      } else {
        row._id = "";
        row.price = 0;
        row.model = "";
      }
      this.rows = [...this.rows];
    });
  }

  // Set build as finished
  finishBuild() {
    if (this.validBuild) {
      this.userService.finishBuild(this.build).subscribe(res => {
        this.snackBar.open("Success", "", {
          duration: 500,
        })
      });
    }
  }

  saveBuild() {
    if (this.name.nativeElement.value !== this.build.name || this.desc.nativeElement.value !== this.build.description) {
      this.userService.updateBuildName(this.name.nativeElement.value, this.desc.nativeElement.value).subscribe(res => {
        this.build.name = this.name.nativeElement.value;
        this.build.description = this.desc.nativeElement.value;
      });
    }
  }

  changeTab(e) {
    if (e.index === 1) {
      this.userService.getUserFinishedBuilds().subscribe(builds => {
        this.finishedBuilds = builds;
      })
    }
  }

}
