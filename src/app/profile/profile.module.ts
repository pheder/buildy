import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import {AppRoutingModule} from '../app-routing.module'
import { MyBuildsComponent } from './mybuilds/mybuilds.component';
import {MatButtonModule, MatInputModule,MatIconModule,MatDialogModule,MatProgressBarModule,MatTabsModule, MatSnackBarModule} from '@angular/material';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { SharedModule } from '../common/common.module';

/*
  This module contains all profile related components
*/

@NgModule({
  declarations: [MyBuildsComponent],
  imports: [
    CommonModule,
    AppRoutingModule,
    MatButtonModule,
    MatInputModule,
    MatIconModule,
    MatDialogModule,
    MatTabsModule,
    MatSnackBarModule,
    FormsModule,
    MatProgressBarModule,
    NgxDatatableModule,
    SharedModule
  ],
  exports: [
    MyBuildsComponent
  ]
})
export class ProfileModule { }
