import {
  NgModule
} from '@angular/core';
import {
  CommonModule
} from '@angular/common';

import {
  ComponentsRoutingModule
} from './components-routing.module'

import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import {
  MatButtonModule,
  MatMenuModule,
  MatInputModule,
  MatToolbarModule,
  MatIconModule,
  MatDialogModule,
  MatProgressSpinnerModule,
  MatProgressBarModule,
  MatTabsModule,
  MatSlideToggleModule,
  MatCardModule,
  MatSnackBarModule,
  MatSelectModule,
  MatExpansionModule,
  MatListModule,
  MatCheckboxModule,
  MatAutocompleteModule,
  MatChipsModule
} from '@angular/material';
import {
  HttpClientModule
} from '@angular/common/http';
import {
  FormsModule
} from '@angular/forms';
import {
  ComponentComponent
} from './component/component.component';
import { DetailsComponent } from './details/details.component';
import { CommentComponent } from './comment/comment.component';


@NgModule({
  declarations: [ DetailsComponent,ComponentComponent,CommentComponent],
  imports: [
    CommonModule,
    ComponentsRoutingModule,
    HttpClientModule,
    NgxDatatableModule,
    MatButtonModule,
    MatMenuModule,
    MatInputModule,
    MatToolbarModule,
    MatIconModule,
    MatProgressBarModule,
    MatTabsModule,
    MatDialogModule,
    MatSnackBarModule,
    MatProgressSpinnerModule,
    MatCardModule,
    MatSlideToggleModule,
    MatSelectModule,
    FormsModule,
    MatExpansionModule,
    MatListModule,
    MatCheckboxModule,
    MatAutocompleteModule,
    MatChipsModule
  ],
  exports: []
})
export class ComponentsModule {}
