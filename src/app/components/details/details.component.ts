import { Component, OnInit, ViewEncapsulation, OnDestroy } from '@angular/core';
import { DatePipe } from '@angular/common';
import { BaseComponent } from 'src/app/models/base-component';
import { ActivatedRoute, Router } from '@angular/router';
import { Cpu } from 'src/app/models/components/cpu';
import { ComponentsService } from 'src/app/services/components/components.service';

import { Chart } from "frappe-charts/dist/frappe-charts.esm.js"


@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss'],
  providers: [DatePipe],
  encapsulation: ViewEncapsulation.None
})
export class DetailsComponent implements OnInit, OnDestroy {

  type:string;
  id:string;
  selectedImage:string;
  keys: string[];
  columns:object[] = [];
  rows = [];
  loadingIndicator: boolean = true;
  chartRef;

  component:BaseComponent;

  constructor(private route: ActivatedRoute, private compService: ComponentsService,private router: Router, private datePipe:DatePipe) {
   this.component = new Cpu();
   this.route.paramMap.subscribe( params => {
    this.type = params.get('type');
    this.id = params.get('id');
    });
  }

  ngOnInit() {
    let data = {
      labels: [],
      datasets: []
    };

    let dp = this.datePipe;

    // Load all component information from the database
    this.compService.getComponent(this.id, this.type).subscribe( res => {
       this.component = res;
       this.selectedImage = 'url("assets/images/components/'+ res.images[0] + '")';
       this.keys = Object.keys(res).filter( function(val){return ['_id', 'priceHistory', 'retailers', 'images', '__v', 'comments'].indexOf(val) == -1});
       this.rows = res.retailers;

      // Process price history to generate the chart
       this.component.priceHistory.forEach(function(hist){
         data.labels.push(dp.transform(hist.day, 'shortDate', '+0100'));
         hist.retailers.forEach( ret => {
           var current = data.datasets.find(o => o.name === ret.name);
           if(!current){
             data.datasets.push({name: ret.name, chartType: 'line', color: ['green', 'blue', 'violet', 'red', 'orange','yellow', 'light-blue', 'light-green', 'purple', 'magenta', 'grey', 'dark-grey'][Math.floor((Math.random()*12))], values: [ret.price]});
           }
           else{
             current.values.push(ret.price);
           }
         })
       });
      if(data.datasets.length > 0){
        setTimeout( () => {
          this.chartRef = new Chart('#chart',{
            title: "Historial de precio",
            data: data,
            type: 'line',
            height: 250
            });
        }, 500)
        
      }
     });
  }

  ngOnDestroy(){
    this.chartRef.unbindWindowEvents()
  }

  // Set the main image of the gallery
  setMainImg(src){
    this.selectedImage = 'url("assets/images/components/' + src + '")';
  }

  // Action on row click
  onActivate($event){
    if($event.type  == 'click'){
      window.open($event.row.url, '_blank');
    }
  }

}
