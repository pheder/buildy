import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ComponentComponent } from './component/component.component';
import {DetailsComponent  } from './details/details.component';


const routes: Routes = [
  {  path: ':type',   component: ComponentComponent  },
  { path: ':type/:id',  component: DetailsComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ComponentsRoutingModule { }