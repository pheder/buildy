import { Component, ViewEncapsulation, OnInit, TemplateRef, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { ComponentsService } from '../../services/components/components.service';
import { UserService } from '../../services/user/user.service';

import { MatSnackBar } from '@angular/material';


import { BaseComponent } from '../../models/base-component';
import { Cpu, Gpu, Drive, Mobo, Memory, Psu, Case, CpuCooler } from '../../models/components';
import { Retailer } from '../../models/retailer';
import {CONSTANTS} from '../constants'
import { fromEvent } from 'rxjs';
import {debounceTime} from 'rxjs/operators';


@Component({
  selector: 'app-component',
  templateUrl: './component.component.html',
  styleUrls: ['./component.component.scss'],
  encapsulation: ViewEncapsulation.None,
  host: {
    '(window:resize)': 'onResize($event)'
  }
})



export class ComponentComponent implements OnInit {

  sockets: string[];
  chipsets: string[];
  filterManufacturers: string[];
  componentForm: BaseComponent;
  filters: any;
  newRetailer: Retailer = new Retailer();
  constants : any = CONSTANTS;
  type: string;
  showAddForm: boolean;

  @ViewChild('manuTmpl') manuTmpl: TemplateRef<any>;
  @ViewChild('controlTmpl') controlTmpl: TemplateRef<any>;
  @ViewChild('hdrTpl') hdrTpl: TemplateRef<any>;
  @ViewChild('table') table: any;
  @ViewChild('model') model:ElementRef;

  columns: object[];
  loggedIn: boolean;
  rows = [];
  temp = [];

  loadingIndicator: boolean = true;
  reorderable: boolean = true;
  actualWidth: Number;

  page: any = {
    offset: 0
  };

  constructor(protected elem: ElementRef, protected route: ActivatedRoute, protected compService: ComponentsService, protected router: Router, protected userService: UserService, protected snackBar: MatSnackBar) {
  }

  ngOnInit() {
   
    // Set options from url parameters
    this.route.paramMap.subscribe(params => {
      this.showAddForm = false;
      this.type = params.get('type');
      if (CONSTANTS.validTypes.indexOf(this.type) === -1) this.router.navigateByUrl('/');
      this.getFormDom(this.type);
      this.initialize();
    });

    // Checks if user is logged in
    this.userService.loggedIn.subscribe(val => {
      this.loggedIn = val;
    });

    // Set debounce for filter text input
    fromEvent(this.model.nativeElement, 'keyup').pipe(debounceTime(1000)).subscribe( () => 
    {
      this.filters.model = this.model.nativeElement.value;
      this.setDataRows();
    })
  }

  initialize() {
    // Initialize table columns
    this.columns = [];
    this.columns.push({ name: 'manufacturer', cellTemplate: this.manuTmpl, headerTemplate: this.hdrTpl });
    this.generateTable();

    // Load table content
    this.setDataRows()
  }

  setDataRows(pageInfo = this.page){
    this.compService.getComponents(this.type, {page: pageInfo.offset, ...this.filters}).subscribe((data) => {
      this.rows = data.rows;
      this.page['totalPages'] = data.totalPages;
      this.page['totalElements'] = data.totalDocs;
      this.page['offset'] = data.page;
      this.loadingIndicator = false;
      this.actualWidth = 800;
      var t = this.table;
      setTimeout(function() {
        window.dispatchEvent(new Event('resize'));
        t.recalculate();
      }, 200);
    });
  }

  // Update table with filters
  updateFilter() {
    this.setDataRows()
  }

  // Generates new component object
  newComponent() {
    this.componentForm =  this.componentForm.constructor();
    this.showAddForm = true;
  }

  // Insert new component
  InsertComponent() {
    if (this.componentForm instanceof Cpu) {
      this.componentForm.multithreading = (this.componentForm.threads > this.componentForm.cores);
    }

    this.componentForm.price = 100000;
    this.componentForm.retailers.forEach(retailer => {
      if (retailer.price < this.componentForm.price) this.componentForm.price = retailer.price;
    });

    if (!this.componentForm._id) {
      // create component
      this.compService.addComponent(this.componentForm, this.type).subscribe(res => {
        this.rows.push(res);
        this.rows = [...this.rows];
        this.componentForm = new BaseComponent();
        this.showAddForm = false;
        this.snackBar.open("Component added successfuly", "", {
          duration: 500,
        });
      });

    }
    else {
      //update component
      this.compService.updateComponent(this.componentForm, this.type).subscribe(res => {
        this.rows = [...this.rows];
        this.componentForm = new BaseComponent();
        this.showAddForm = false;
        this.snackBar.open("Component updated successfuly", "", {
          duration: 500,
        });
      });
    }

  }

  loadSockets() {
    if (this.componentForm.manufacturer == 'intel') {
      this.sockets = CONSTANTS.intelSockets;
    }
    else {
      this.sockets = CONSTANTS.amdSockets;
    }
  }

  onActivate($event) {
    if ($event.type == 'click') {
      this.router.navigateByUrl('components/' + this.type + '/' + $event.row._id);
    }
  }

  // Show edit component form
  editComponent(row, event) {
    event.stopPropagation();
    event.preventDefault();
    this.componentForm = row;
    if(this.type == 'cpu'){
      this.loadSockets();
    }
    let scrollToTop = window.setInterval(() => {
      let pos = window.pageYOffset;
      if (pos > 0) {
          window.scrollTo(0, pos - 20);
      } else {
          window.clearInterval(scrollToTop);
      }
      }, 16);
    this.showAddForm = true;
  }

  // Adds new component to the user's build
  addComponent(row, event) {
    event.stopPropagation();
    event.preventDefault();
    this.userService.updateBuild(this.type, row).subscribe(res => {
      this.router.navigate(['/my-builds']);
    });
  }


  validateRetailer() {
    if (!this.newRetailer.url || this.newRetailer.url == '') {
      return;
    }
    this.newRetailer.name = this.getRetailerFromUrl(this.newRetailer.url);
    if (!!this.newRetailer.name && this.newRetailer.name != '') {
      this.compService.validateRetailer(this.newRetailer).subscribe(res => {
        let copy = JSON.parse(JSON.stringify(this.newRetailer));
        copy.price = parseFloat(res);
        if (copy.price < this.componentForm.price) this.componentForm.price = copy.price;
        this.componentForm.retailers.push(copy);
        this.newRetailer = new Retailer();
      });
    }
    else {
      console.log("Invalid retailer");
    }

  }

  deleteRetailer(i) {
    this.componentForm.retailers.splice(i, 1);
  }

  getRetailerFromUrl(url) {
    var hostname = new URL(url).hostname;
    return CONSTANTS.retailerMap[hostname];

  }

  getFormDom(type) {
    this.filters = {'priceOnly': true};
    switch (type) {
      case 'cpu':
        this.componentForm = new Cpu();
        this.filterManufacturers = CONSTANTS.cpuManufacturers;
        break;
      case 'gpu':
        this.componentForm = new Gpu();
        this.filterManufacturers =CONSTANTS.gpuManufacturers;
        break;
      case 'drive':
        this.componentForm = new Drive();
        this.filterManufacturers = CONSTANTS.driveManufacturers;
        break;
      case 'motherboard':
        this.componentForm = new Mobo();
        this.sockets = [...CONSTANTS.intelSockets,  ...CONSTANTS.amdSockets];
        this.chipsets = [...CONSTANTS.intelChipsets,  ...CONSTANTS.amdChipsets];
        this.filterManufacturers = CONSTANTS.moboManufacturers;
        break;
      case 'memory':
        this.componentForm = new Memory();
        this.filterManufacturers = CONSTANTS.memoryManufacturers;
        break;
      case 'psu':
        this.componentForm = new Psu();
        this.filterManufacturers = CONSTANTS.psuManufacturers;
        break;
      case 'cpu-cooler':
        this.componentForm = new CpuCooler();
        this.filterManufacturers = CONSTANTS.cpuCoolerManufacturers;
        this.sockets = [...CONSTANTS.intelSockets,  ...CONSTANTS.amdSockets];
        break;
      case 'case':
        this.componentForm = new Case();
        this.filterManufacturers = CONSTANTS.caseManufacturers;
        break;
      default:
        break;
    }
  }

  generateTable() {
    Object.keys(this.componentForm).forEach(prop => {
      if (prop != 'retailers' && prop != 'manufacturer' && prop != 'images' && prop != 'priceHistory' && prop != 'wifi') {
        this.columns.push({ name: prop, prop: prop, headerTemplate: this.hdrTpl });
      }
    });
    this.columns.push({ name: 'options', cellTemplate: this.controlTmpl, headerTemplate: this.hdrTpl });
  }

  onSubmitImage() {
    let formData = new FormData();
    let file = this.elem.nativeElement.querySelector("#file").files[0];
    formData.append('file', file, file.name);
    this.compService.uploadImage(formData).subscribe(res => {
      this.componentForm.images.push(res.src);
    });
  }

  selectedFile: string = "";

  changeFile(e) {
    this.selectedFile = e.target.files[0].name;
  }

  onResize(event) {
    let windowidth = event.target.innerWidth;
    if (windowidth < 577 && this.actualWidth >= 577) {
      this.table.rowDetail.expandAllRows();
      this.actualWidth = 500;
    }
    else if (windowidth > 576 && this.actualWidth <= 577) {
      this.table.rowDetail.collapseAllRows();
      this.actualWidth = 600;
    }
  }

  remove(ele,array): void {
    let index = array.indexOf(ele);
    if (index >= 0) {
      array.splice(index, 1);
    }
  }

  addNewSocket(e,arr){
    var element = e.option.value;
    if(arr.indexOf(element) === -1){
      arr.push(element);
    }
  }

}
