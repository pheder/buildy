import { Component, OnInit, Input } from '@angular/core';
import { ComponentsService } from '../../services/components/components.service';
import { UserService } from '../../services/user/user.service';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.scss']
})
export class CommentComponent implements OnInit {

  @Input() comments;
  @Input() type;
  @Input() componentId;

  commentDescription : String;
  user: Object;

  constructor(private compService: ComponentsService, private userService: UserService) { }

  ngOnInit() {
    this.user = this.userService.getUser();
  }

  addComment(){
    this.compService.addComment(this.componentId, this.type, this.commentDescription, this.user).subscribe(res => {
      this.comments.push(res);
      this.commentDescription = '';
    });
  }

  likeComment(comment){
    let index = comment.rating.indexOf(this.user["_id"]);
    if(index == -1){
      comment.rating.push(this.user["_id"]);
    }
    else{
      comment.rating.splice(index,1);
    }
    this.compService.likeComment(this.componentId, this.type, comment, this.user["_id"]).subscribe(res => {
    });
  }

  getThumbColor(ratings){
    if(this.user && ratings.indexOf(this.user["_id"]) == -1){
      return 'primary';
    }
    else{
      return 'warning';
    }
  }

}
