import { Injectable } from '@angular/core';

import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import {JwtHelperService} from '@auth0/angular-jwt';

@Injectable()
export class Guard implements CanActivate {

  constructor(private router: Router, private jwtHelper:JwtHelperService) { }

  // Determines if the user has a valid token to display a protected area
  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    let token = localStorage.getItem('token');
    if (!token || this.jwtHelper.isTokenExpired(localStorage.getItem('token'))) {
      this.router.navigate(['/login']);
      return false;
    }
    else {
      return true;
    }
  }
}
