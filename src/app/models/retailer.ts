
export class Retailer{
    name:string;
    url: string;
    price: number;
    stock: boolean;
    constructor(name?: string, url?:string, price?:number){
      this.name = name || "";
      this.url = url || "";
      this.price = price || 0;
      this.stock = true;
    }
    reset(){
      this.name = "";
      this.url = "";
      this.price = 0;
      this.stock = true;
    }
  }
  