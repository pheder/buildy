import {BaseComponent} from '../base-component';

export class Case extends BaseComponent {
  type: string;
  exBig: number;
  inBig: number;
  inSmall: number;
  cardSize: number;
  constructor(){
    super();
    this.type = "";
    this.exBig = 0;
    this.inBig = 0;
    this.inSmall = 0;
    this.cardSize = 0;
  }
}