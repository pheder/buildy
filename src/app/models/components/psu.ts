import {BaseComponent} from '../base-component';

export class Psu extends BaseComponent {
  type: string;
  efficiency: string;
  modular: string;
  power: number;
  constructor(){
    super();
    this.type = "";
    this.efficiency = "";
    this.modular = "";
    this.power = 0;
  }
}