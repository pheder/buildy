import {BaseComponent} from '../base-component';

export class Gpu extends BaseComponent {
  chipset: string;
  memory_size: number;
  memory_type: string;
  ports: string;
  clock: number;
  tdp: number;
  sync: string;
  constructor(){
    super();
    this.clock = 0;
    this.chipset = "";
    this.memory_size = 0;
    this.memory_type = "";
    this.tdp = 0;
    this.ports = "";
    this.sync = "";
  }
}