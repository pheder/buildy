import {BaseComponent} from '../base-component';

export class Drive extends BaseComponent {
  size: string;
  type: string;
  capacity: number;
  interface: string;
  constructor(){
    super();
    this.size = "";
    this.type = "";
    this.capacity = 0;
    this.interface = "";
  }
}