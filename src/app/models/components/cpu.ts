import {BaseComponent} from '../base-component';

export class Cpu extends BaseComponent {
    socket: string;
    clock: number;
    cores: number;
    threads: number;
    tdp: number;
    igpu: string;
    multithreading: boolean;
    constructor(){
      super();
      this.clock = 0;
      this.socket = "";
      this.cores = 0;
      this.threads = 0;
      this.tdp = 0;
    }
  }