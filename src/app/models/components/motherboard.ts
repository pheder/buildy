import {BaseComponent} from '../base-component';

export class Mobo extends BaseComponent {
  //socket: string;
  form: string;
  chipset: string;
  ram_slots: number;
  ram_size: number;
  ram_type: string;
  sata: number;
  wifi: boolean;
  m2: number;
  pcie: number;
 // audio: string;
  constructor(){
    super();
   // this.socket = "";
    this.form = "";
    this.chipset = "";
    this.ram_slots = 0;
    this.ram_size = 0;
    this.ram_type = "";
    this.sata = 0;
    this.wifi = false;
    this.m2 = 0;
    this.pcie = 0;
   // this.audio = "";
  }
}