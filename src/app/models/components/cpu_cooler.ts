import {BaseComponent} from '../base-component';

export class CpuCooler extends BaseComponent {
  noise: string;
  speed: string;
  liquid: boolean;
  sockets: string[];
  constructor(){
    super();
    this.noise = "";
    this.speed = "";
    this.liquid = false;
    this.sockets = []
  }
}
