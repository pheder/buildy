import {BaseComponent} from '../base-component';

export class Memory extends BaseComponent {
  size: number;
  type: string;
  speed: string;
  constructor(){
    super();
    this.type = "";
    this.size = 0;
    this.speed = "";
  }
}