import {Retailer} from './retailer';

export class BaseComponent{
  _id:string;
  manufacturer: string;
  model: string;
  retailers: Retailer[];
  images: string[];
  price: number;
  priceHistory: any[];
  comments: any[];
  constructor(){
    this.manufacturer = "";
    this.model = "";
    this.price = 0;
    this.priceHistory = [];
    this.retailers = [];
    this.images = [];
  }
}