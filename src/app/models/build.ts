
export class Build  {
  name: string;
  description: string;
  cpu: {manufacturer: string, model: string, price: number};
  cpu_cooler: {manufacturer: string, model: string, price: number};
  gpu: {manufacturer: string, model: string, price: number};
  case: {manufacturer: string, model: string, price: number};
  drive: [{manufacturer: string, model: string, price: number}];
  memory: {manufacturer: string, model: string, price: number};
  psu: {manufacturer: string, model: string, price: number};
  mobo: {manufacturer: string, model: string, price: number};
  price: number;
  images: [string];
  owner: {_id : string, name: string};
  constructor(){
    this.name = "Mi build";
    this.description = "Best build ever";
    this.owner = {_id : "", name: ""};
  }
}