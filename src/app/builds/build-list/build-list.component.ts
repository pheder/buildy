import { Component, OnInit } from '@angular/core';
import { BuildService } from 'src/app/services/build/build.service';

@Component({
  selector: 'app-build-list',
  templateUrl: './build-list.component.html',
  styleUrls: ['./build-list.component.scss']
})
export class BuildListComponent implements OnInit {

  filters: any = {page: 0};
  builds: any[];

  constructor(private buildService: BuildService) { }

  ngOnInit() {
    // Load paginated builds from the database
    this.buildService.getBuilds(this.filters).subscribe(data => {
      this.builds = data.rows;
    })
  }

}
