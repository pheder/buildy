import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {BuildsRoutingModule} from './builds-routing.module'
import { BuildListComponent } from './build-list/build-list.component';
import { BuildService } from '../services/build/build.service'
import { SharedModule } from '../common/common.module';

@NgModule({
  declarations: [BuildListComponent],
  imports: [
    CommonModule,
    BuildsRoutingModule,
    SharedModule
  ],
  exports: [
    BuildListComponent
  ],
  providers: [BuildService]
})
export class BuildsModule { }
