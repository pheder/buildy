import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {JwtHelperService} from '@auth0/angular-jwt';
import { Observable, BehaviorSubject } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable()
export class UserService{

  private token = null;
  private isLogged:BehaviorSubject<boolean> ;
  private user:any; 

  public loggedIn:Observable<boolean>;
  public server:string = environment.apiRoot;

  constructor(private http: HttpClient, private jwtHelper: JwtHelperService ) {
    this.token = localStorage.getItem('token');
    let validToken = this.token && !this.jwtHelper.isTokenExpired(this.token);

    // Declares a stream that "listens" for login / logout changes
    this.isLogged = new BehaviorSubject<boolean>(validToken);
    this.loggedIn= this.isLogged.asObservable();

    // If the user is valid keeps the information locally
    if(validToken){
      this.setUser(jwtHelper.decodeToken(this.token).user);
    }
  }

  // Stores an user
  setUser(user){
    this.user = user;
  }

  // Returns the current user
  getUser():object{
    return this.user;
  }

  // Returns the current user name
  getUserName():string{
        return this.user.username;
  }

  // Returns if the user is logged in
  isLoggedIn(){
    let validToken = (this.token && !this.jwtHelper.isTokenExpired(this.token));
    this.isLogged.next(validToken);
    return validToken;
  }

  // Request login access to the server
  login(form): Observable<any> {
    return this.http.post(this.server + '/api/login', form);
  }

  // Indicates to the stream that the user is logged in
  signIn(){
    this.isLogged.next(true);
  }

  // Logout the user
  logOut(){
    localStorage.clear();
    this.isLogged.next(false);
  }

  // Requests a register to the server
  register(form): Observable<any> {
    return this.http.post(this.server + '/api/user', form);
  }

  // Update user's build component in construction
  updateBuild(type, component): Observable<any> {
    return this.http.post(this.server + '/api/user/' + this.user._id + '/build', {component: component, type: type});
  }

  // removes component from user's build in construction
  deleteBuildComponent(type, componentId): Observable<any> {
    return this.http.delete(this.server + '/api/user/' + this.user._id + '/build/' + type +'/' + componentId);
  }

  // Update user's build name in construction
  updateBuildName(name,description): Observable<any> {
    return this.http.post(this.server + '/api/user/' + this.user._id + '/buildName', {name: name, description: description});
  }

  // Returns user's build in construction from the database
  getUserBuild():Observable<any> {
    return this.http.get(this.server + '/api/user/' + this.user._id + '/build');
  }

  // Sets build to finised
  finishBuild(build):Observable<any> {
    build.owner._id = this.user._id;
    build.owner.name = this.user.username;
    return this.http.post(this.server + '/api/user/' + this.user._id + '/build/finish', build);
  }

  // Returns all user finished builds
  getUserFinishedBuilds():Observable<any> {
    return this.http.get(this.server + '/api/build/user/' + this.user._id );
  }


}
