import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';

@Injectable()
export class ComponentsService{

  private options;

  constructor(private http: HttpClient,) {
    this.options = {headers:  new HttpHeaders({ 'Content-Type': 'application/json'})};
  }

  server:string = environment.apiRoot;

  // Get components filtered
  getComponents(type, filters = {}): Observable<any> {
    return this.http.get(this.server + '/api/'+type, {params: filters});
  }

  // Count total components
  countComponents(): Observable<any> {
    return this.http.get(this.server + '/api/components/count');
  }

  // Insert a component in the database
  addComponent(component, type): Observable<any> {
    return this.http.post(this.server + '/api/'+ type, JSON.stringify(component), this.options);
  }

  // Get a single component by type and id
  getComponent(componentId, type): Observable<any> {
    return this.http.get(this.server + '/api/'+ type +'/' + componentId);
  }

  // Update a single component by type and id
  updateComponent(component, type): Observable<any> {
    var compId = component._id;
    delete component._id;
    return this.http.put(this.server + '/api/' + type +'/' + compId , JSON.stringify(component), this.options);
  }

  // Add a new comment in the database
  addComment(componentId, type, commentDescription, user): Observable<any> {
    return this.http.post(this.server + '/api/'+ type +'/' + componentId + '/comment', JSON.stringify({description : commentDescription, author: user._id}), this.options);
  }

  // Save toggle like button
  likeComment(componentId, type, comment, userid): Observable<any> {
    return this.http.put(this.server + '/api/'+ type +'/' + componentId + '/comment/' + comment._id, JSON.stringify({userid : userid}), this.options);
  }

  // Delete a component from the database by id
  deleteComponent(component): Observable<any> {
    return this.http.delete(this.server + `/api/components/${component._id}`, this.options);
  }

  // Validates that the retailer introduced is correct
  validateRetailer(retailer):Observable<any> {
    return this.http.post(this.server + '/api/components/validate', JSON.stringify(retailer), this.options);
  }

  // Upload an image to the server
  uploadImage(file): Observable<any>{
    return this.http.post(this.server + '/api/components/upload', file);
  }

   // Get all builds from the database
  getBuilds(): Observable<any> {
    return this.http.get(this.server + '/api/builds');
  }
}
