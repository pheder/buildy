import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BuildService {

  private options;

  constructor(private http: HttpClient,) {
    this.options = {headers:  new HttpHeaders({ 'Content-Type': 'application/json'})};
  }

  server:string = environment.apiRoot;

  // Return paginated builds from the database
  getBuilds(filters = {}): Observable<any> {
    return this.http.get(this.server + '/api/build', {params: filters});
  }
}
