import {
  NgModule
} from '@angular/core';
import {
  CommonModule
} from '@angular/common';
import {
  FormsModule
} from '@angular/forms';

import {
  AppRoutingModule
} from '../app-routing.module'

import {
  LoginComponent
} from './login/login.component';

import {
  MatButtonModule,
  MatInputModule,
  MatIconModule,
  MatDialogModule,
  MatProgressBarModule,
  MatTabsModule,
} from '@angular/material';

@NgModule({
  declarations: [LoginComponent],
  imports: [
    CommonModule,
    AppRoutingModule,
    MatButtonModule,
    MatInputModule,
    MatIconModule,
    MatDialogModule,
    MatTabsModule,
    FormsModule,
    MatProgressBarModule
  ],
  exports: [
    LoginComponent
  ]
})
export class AuthModule {}