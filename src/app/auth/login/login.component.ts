import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators, FormBuilder,EmailValidator } from '@angular/forms';
import { UserService } from './../../services/user/user.service';


import {MatSnackBar, MatDialogRef,  MatTabGroup} from '@angular/material';

@Component({
  selector: 'login-component',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})



export class LoginComponent implements OnInit {

  tabGroup:MatTabGroup;

  loading:boolean = false;
  loginForm: any;
  registerForm: any;
  email = new FormControl('', [Validators.required,
                               Validators.minLength(3),
                               Validators.maxLength(100)]);
  password = new FormControl('', [Validators.required,
                                  Validators.minLength(6)]);

  constructor( private router: Router,
              private userService: UserService,
            public snackBar: MatSnackBar,
            public dialogRef: MatDialogRef<LoginComponent>,) { }

  ngOnInit() {
    // this.loginForm = this.formBuilder.group({
    //   email: this.email,
    //   password: this.password
    // });
    this.loginForm = {};
    this.registerForm = {role:"user"};
  }

  setClassEmail() {
    return { 'has-danger': !this.email.pristine && !this.email.valid };
  }
  setClassPassword() {
    return { 'has-danger': !this.password.pristine && !this.password.valid };
  }

  signIn() {
    this.loading = true;
    this.userService.login(this.loginForm).subscribe(
      res => {
        localStorage.setItem('token', res.token);
        this.userService.setUser(res.user);
        this.userService.signIn();
        this.loading = false;
        this.dialogRef.close();
        this.snackBar.open("Success", "", {
          duration: 500,
        })
      },
      error =>  {
        this.loading = false;
        this.snackBar.open('error', '', {
          duration: 3000
        });
      },
      () => { this.loading = false; }
    );
  };

  newUser() {
    this.loading = true;
    this.userService.register(this.registerForm).subscribe(
      res => {
        this.loading = false;
        this.dialogRef.close();
        this.snackBar.open("Success", "", {
          duration: 500,
        })
      //  this.tabGroup.selectedIndex = 1;
      },
      error =>  {
        this.loading = false;
        this.snackBar.open('error', '', {
          duration: 3000
        })
      }
    );
  };

}
