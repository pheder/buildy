import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-build-group',
  templateUrl: './build-group.component.html',
  styleUrls: ['./build-group.component.scss']
})
export class BuildGroupComponent  {

  @Input('builds') builds: any[];

  constructor() { }

}
