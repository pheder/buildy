import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BuildGroupComponent } from './components/build-group/build-group.component';

import {MatIconModule, MatButtonModule} from '@angular/material'

/*
  This module loads components, classes and services shared across diferent modules
 */

@NgModule({
  declarations: [BuildGroupComponent],
  imports: [
    CommonModule,
    MatIconModule,
    MatButtonModule,
  ],
  exports: [
    BuildGroupComponent
  ],
  providers: []
})
export class SharedModule { }
