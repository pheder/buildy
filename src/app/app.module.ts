// Angular dependencies
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';

// Module imports
import {MatButtonModule, MatMenuModule, MatInputModule,  MatToolbarModule,MatIconModule,MatDialogModule,MatProgressBarModule,MatTabsModule, MatSnackBarModule} from '@angular/material';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { JwtModule } from '@auth0/angular-jwt';
import { LayoutModule} from './layout/layout.module';
import {AuthModule} from './auth/auth.module';
import {ProfileModule} from './profile/profile.module'

// Components and services
import { AppComponent } from './app.component';
import { UserService } from './services/user/user.service';
import { ComponentsService } from './services/components/components.service';
import {Guard} from './auth.guard';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    LayoutModule,
    AuthModule,
    ProfileModule,
    HttpClientModule,
    MatButtonModule,
    MatMenuModule,
    MatInputModule,
    MatToolbarModule,
    MatIconModule,
    MatProgressBarModule,
    MatTabsModule,
    MatDialogModule,
    MatSnackBarModule,
    FormsModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: getJwtToken,
        whitelistedDomains: ['localhost:3000', 'buildy.tk']
      }
    })
  ],
  providers: [UserService, ComponentsService, Guard ],
  bootstrap: [AppComponent]
})
export class AppModule { }

export function getJwtToken(): string {
  return localStorage.getItem('token');
}

