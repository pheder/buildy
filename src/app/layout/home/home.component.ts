import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { UserService } from 'src/app/services/user/user.service';
import { LoginComponent } from 'src/app/auth/login/login.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  loggedIn:boolean;

  constructor(public dialog: MatDialog, private userService:UserService, private router:Router) { }

  ngOnInit(){
    this.userService.isLoggedIn();
    this.userService.loggedIn.subscribe(val => {
       this.loggedIn = val;
    });
  }

  openLogin(): void {
    if(this.loggedIn){
      this.router.navigate(['my-builds'])
    }
    else{
      let loginDialogRef = this.dialog.open(LoginComponent, {
        width: '380px',
        disableClose: false
      });
  
      loginDialogRef.afterClosed().subscribe(result => {
        
      });
    }
  }

}
