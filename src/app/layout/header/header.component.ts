import { Component, OnInit } from '@angular/core';
import {MatDialog} from '@angular/material';
import { Router } from '@angular/router';

import {LoginComponent} from '../../auth/login/login.component'
import { UserService } from '../../services/user/user.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit{
  loggedIn:boolean;
  username:string;
  showMenu:boolean;

  // List of components
  componentList: object[] = [
    {name: 'Procesadores', url: 'cpu', icon: 'flaticon-017-processor'},
    {name: 'Tarjetas Gráficas', url: 'gpu', icon: 'flaticon-004-video-card'},
    {name: 'Fuentes de Alimentación', url: 'psu', icon: 'flaticon-018-power-source'},
    {name: 'Memorias RAM', url: 'memory', icon: 'flaticon-013-ram'},
    {name: 'Cajas', url: 'case', icon: 'flaticon-019-computer'},
    {name: 'Almacenamiento', url: 'drive', icon: 'flaticon-041-hard-disk'},
    {name: 'Placas Base', url: 'motherboard', icon: 'flaticon-025-usb'},
    {name: 'Disipadores de CPU', url: 'cpu-cooler', icon: 'flaticon-054-cooler'},
  ];
  constructor(public dialog: MatDialog, private userService:UserService, private router:Router) {
  
  }

  ngOnInit(){
    // Check if user is logged in and retrive username
    this.userService.isLoggedIn();
    this.userService.loggedIn.subscribe(val => {
       this.loggedIn = val;
       if(val){
           this.username = this.userService.getUserName();
       }
    });
  }

  toggleMenu(): void{
    this.showMenu = !this.showMenu ;
  }

  // Show login / register modal
  openLogin(): void {
      let loginDialogRef = this.dialog.open(LoginComponent, {
        width: '380px',
        disableClose: false
      });
  
      loginDialogRef.afterClosed().subscribe(result => {
        
      });
  }

  logOut(): void{
    this.userService.logOut();
    this.loggedIn = false;
  }

}