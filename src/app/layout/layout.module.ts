import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import {AuthModule} from '../auth/auth.module';
import { AppRoutingModule } from './../app-routing.module';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import {MatButtonModule, MatMenuModule, MatInputModule,  MatToolbarModule,MatIconModule,MatDialogModule,MatProgressBarModule,MatTabsModule, MatSnackBarModule} from '@angular/material';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { HomeComponent } from './home/home.component';

/*
  This module contains the layout components
*/

@NgModule({
  declarations: [HeaderComponent,FooterComponent, HomeComponent],
  imports: [
    BrowserModule,
    CommonModule,
    AppRoutingModule,
    HttpClientModule,
    AuthModule,
    MatButtonModule,
    MatMenuModule,
    MatInputModule,
    MatToolbarModule,
    MatIconModule,
    MatProgressBarModule,
    MatTabsModule,
    MatDialogModule,
    MatSnackBarModule,
    FormsModule
  ],
  exports: [
    HeaderComponent, FooterComponent
  ]
})
export class LayoutModule { }
