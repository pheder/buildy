import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { LoginComponent} from './auth/login/login.component';
import { MyBuildsComponent} from './profile/mybuilds/mybuilds.component';
import {Guard} from './auth.guard';
import { HomeComponent } from './layout/home/home.component';


// Set main routes of the app
const routes: Routes = [
  { path: '',  component: HomeComponent },
  { path: 'login',  component: LoginComponent },
  { path: 'components',   loadChildren: './components/components.module#ComponentsModule' },
  { path: 'builds',   loadChildren: './builds/builds.module#BuildsModule' },
  { path: 'my-builds',  component: MyBuildsComponent, canActivate: [Guard] },
  { path: '**', redirectTo: '/', pathMatch: 'full' },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes,  {preloadingStrategy: PreloadAllModules}) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
